#!/bin/bash
apt-get install apache2 -y
apt-get install git  -y
rm -rf /var/www/html/*
cd /var/www/html/
git clone https://github.com/GinuMathew/terraform-git.git
mv /var/www/html/terraform-git/* /var/www/html/
sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
sudo apt-get install php7.4 -y
sudo apt-get install php7.4-mysql -y
service apache2 restart
