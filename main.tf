############################
# MAINTAINER - GINU MATHEW #
# Custom VPC, EC2, RDS     #
############################

######## CUSTOM VPC CREATION ##########

resource "aws_vpc" "ginuvpc" {
  cidr_block       = "172.168.0.0/16"
  instance_tenancy = "default"
  enable_dns_support = "true"
  enable_dns_hostnames = "true"

  tags = {
    Name = "ginuvpc"
  }
}


#######################################################
# Public subnet 1 & 2 are in same AZ =  eu-central-1a #
# Private subnet 3 in Az = eu-central-1b              #
#######################################################

##### PUBLIC SUBNET - 1 ####

resource "aws_subnet" "publicsub1" {   
  vpc_id     = aws_vpc.ginuvpc.id
  availability_zone = "eu-central-1a"
  cidr_block = "172.168.0.0/18"
  map_public_ip_on_launch = "true"

  tags = {
    Name = "Publicsub1"
  }
}

##### PUBLIC SUBNET - 2 #####

resource "aws_subnet" "publicsub2" {
  vpc_id     = aws_vpc.ginuvpc.id
    availability_zone = "eu-central-1a"
  cidr_block = "172.168.64.0/18"
  map_public_ip_on_launch = "true"

  tags = {
    Name = "Publicsub2"
  }
}

##### PRIVATE SUBNET #####

resource "aws_subnet" "Privatesub" {
  vpc_id     = aws_vpc.ginuvpc.id
  availability_zone = "eu-central-1b"
  cidr_block = "172.168.128.0/18"

  tags = {
    Name = "Privatesub"
  }
}

#### INTERNET GATEWAY ####

resource "aws_internet_gateway" "ginuig" {
  vpc_id = aws_vpc.ginuvpc.id

  tags = {
    Name = "ginuig"
  }
}


#### ELASTIC IP FOR NAT GATEWAY #####

resource "aws_eip" "ginueip" {
  vpc      = true
}


#### NAT GATEWAY ####

resource "aws_nat_gateway" "ginunatgw" {
  allocation_id = aws_eip.ginueip.id
  subnet_id     = aws_subnet.publicsub2.id

  tags = {
    Name = "ginunatgw"
  }
}



#### ROUTE TABLE FOR PUBLIC 1 & 2 ####

resource "aws_route_table" "ginurt1" {
  vpc_id = aws_vpc.ginuvpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.ginuig.id
  }

  tags = {
  
  Name = "PublicRoute"
  }
  }

#### ROUTE TABLE FOR PRIVATE  ####

resource "aws_route_table" "ginurt2" {
  vpc_id = aws_vpc.ginuvpc.id

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.ginunatgw.id
  }
  tags = {
  
  Name = "PrivateRoute"
  }
  }

#### ROUTE TABLE ASSOCIATION FOR PUBLIC 1 & 2 ####


resource "aws_route_table_association" "ginurta1" {
  subnet_id      = aws_subnet.publicsub2.id
  route_table_id = aws_route_table.ginurt1.id
  }

resource "aws_route_table_association" "ginurta2" {
  subnet_id      = aws_subnet.publicsub1.id
  route_table_id = aws_route_table.ginurt1.id
  }

#### ROUTE TABLE ASSOCIATION FOR PRIVATE ####

resource "aws_route_table_association" "ginurta3" {
  subnet_id      = aws_subnet.Privatesub.id
  route_table_id = aws_route_table.ginurt2.id
}



#### SECURITY GROUP FOR PUB SERVER 1 ####

resource "aws_security_group" "ginussh" {
  name        = "allow_ssh"
  description = "Allow SSH inbound traffic"
  vpc_id      = aws_vpc.ginuvpc.id

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_SSH"
  }
}


#### SECURITY GROUP FOR PUB SERVER 2 - ALLOW ACCESS FROM PUB SERVER 1 ONLY ####

resource "aws_security_group" "ginuserver2ssh" {
  name        = "allow_SSH_pub2"
  description = "Allow SSH inbound traffic"
  vpc_id      = aws_vpc.ginuvpc.id

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    security_groups  =  [ aws_security_group.ginussh.id ]
  }

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
   
   }
    
    ingress {
    description = "HTTPS"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_SSH_from_pub1"
  }
}



#### SECURITY GROUP FOR PRIVATE SERVER - ALLOW ACCESS FROM PUB SERVER 2 ONLY ####

resource "aws_security_group" "ginuserver3ssh" {
  name        = "allow_SSH_priv"
  description = "Allow SSH inbound traffic"
  vpc_id      = aws_vpc.ginuvpc.id

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    security_groups  =  [ aws_security_group.ginuserver2ssh.id ]
  }

  ingress {
    description = "MYSQL"
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
   
   }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_SSH_from_pub2_only"
  }
}


#### AWS KEY PAIR CREATION ####

resource "aws_key_pair" "ginuawskey" {
  key_name   = "ginuawskey"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCdCPhhE8yAJxrmas+WLWj/H84HkKzbst63SVwNHfcMQdGsn14oiH4cVRz0jJBARc+Z7lOR10GbVqI5XWNBpxOSEj9/A6544WhIRR92juXAM+k3CFnKN7qZ9iwTAaGPxcPXnpt2gfMsDL5E/zm7Hk2ay64sbbZ/bgnyCdH64fFC36XxgYxCoh7m3jBmHaCaPW0nibZg9B3XBUgxKGIZIfab8Sd8CqqnH9ZgogwZanikF4UfvMglJ3cLO2IBN2yEqL69+4l1KPjvGtnsCoPSncp0Q10EB8398EThehcMyg6T9XURIa0k0QfUBjJV9oU0ezSeNyMXQ8y7c42VirxlnWwAKkem7ojE2efzjhroFF+aD+E2Zr9r93Rtm2YSl8DoW5sQK8z43nMo5dkROIIFlEGh2F53AqgPywOAdzvTr3raEeyvSRw2MvdrlxNaAd1mnbAcaotCzuA2BdM3hiJig89Ymo8iHr0CFmjSjozgSrujHwc2goEzh/PeN7fBfLcRHjc= root@ip-172-31-7-20"
}



##### AWS EC2 CREATION FOR PUBLIC SERVER 1 #####


resource "aws_instance" "pub1" {
  ami           =  "ami-0502e817a62226e03"
  instance_type = "t3.micro"
  key_name      = aws_key_pair.ginuawskey.id
  subnet_id =  aws_subnet.publicsub1.id
  vpc_security_group_ids = [ aws_security_group.ginussh.id ]
  associate_public_ip_address = "true"

  tags = {
    Name = "Pub_server1"
  }
}


##### AWS EC2 CREATION FOR PUBLIC SERVER 2 #####


resource "aws_instance" "pub2" {
  ami           =  "ami-0502e817a62226e03"
  instance_type = "t3.micro"
  key_name      = aws_key_pair.ginuawskey.id
  subnet_id =  aws_subnet.publicsub2.id
  vpc_security_group_ids = [ aws_security_group.ginuserver2ssh.id ]
  associate_public_ip_address= "true"
  user_data = file ("pub2.sh")
  tags = {
    Name = "Pub_server2"
  }
}

##### AWS EC2 CREATION FOR PRIVATE SERVER #####


  resource "aws_instance" "private" {
  ami           =  "ami-0502e817a62226e03"
  instance_type = "t3.micro"
  key_name      = aws_key_pair.ginuawskey.id
  subnet_id =  aws_subnet.Privatesub.id
  vpc_security_group_ids = [ aws_security_group.ginuserver3ssh.id ]

  tags = {
    Name = "Priv_server"
  }
}





#### RDS instance creation ####

resource "aws_db_instance" "ginurds" {
  allocated_storage    = 20
  storage_type         = "gp2"
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "db.t2.micro"
  name                 = "mydb"
  username             = "root"
  password             = "ginu1234"
  parameter_group_name = "default.mysql5.7"
  
}
